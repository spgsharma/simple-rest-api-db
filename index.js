const mysql = require("mysql2");
const express = require("express");

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "sample_rest_api_db",
});
connection.on("connect", () => {
    console.log("DB connected");
});

const app = express();
app.use(express.json());

app.get("/contacts", (req, res) => {
    connection.query("SELECT * FROM contacts", (err, rows) => {
        if (err) throw err;
        res.json(rows);
    });
});

app.get("/contacts/:id", (req, res) => {
    connection.query(
        "SELECT * FROM contacts WHERE id = ?",
        [req.params.id],
        (err, rows) => {
            if (err) throw err;
            if (rows.length === 0)
                return res.status(404).send({
                    error: {
                        code: "ERROR_NOT_FOUND",
                        message: "Not found",
                    },
                });
            res.json(rows[0]);
        }
    );
});

app.post("/contacts", (req, res) => {
    connection.query(
        "INSERT INTO contacts SET ?",
        { name: req.body.name, email: req.body.email },
        (err, result) => {
            if (err) throw err;
            res.json({
                id: result.insertId,
                name: req.body.name,
                email: req.body.email,
            });
        }
    );
});

app.put("/contacts/:id", (req, res) => {
    connection.query(
        "UPDATE contacts SET name = ?, email = ? WHERE id = ?",
        [req.body.name, req.body.email, req.params.id],
        (err, result) => {
            if (err) throw err;
            if (result.affectedRows === 0)
                return res.status(404).send({
                    error: {
                        code: "ERROR_NOT_FOUND",
                        message: "Not found",
                    },
                });
            res.json({
                id: req.params.id,
                name: req.body.name,
                email: req.body.email,
            });
        }
    );
});

app.delete("/contacts/:id", (req, res) => {
    connection.query(
        "DELETE FROM contacts WHERE id = ?",
        [req.params.id],
        (err, result) => {
            if (err) throw err;
            if (result.affectedRows === 0)
                return res.status(404).send({
                    error: {
                        code: "ERROR_NOT_FOUND",
                        message: "Not found",
                    },
                });
            res.status(200).send();
        }
    );
});

app.listen(3000, () => {
    console.log("App listening on port 3000");
});
